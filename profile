source ~/.dotfiles/aliases

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
export EDITOR=vim

export PATH="$HOME/.dotfiles/scripts:/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH"
