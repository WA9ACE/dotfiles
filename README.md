# My Dot Files

## Setup
One Line

	curl -fsSL https://raw.githubusercontent.com/C0deMaver1ck/dotfiles/master/bootstrap | bash

Download

	git clone https://github.com/C0deMaver1ck/dotfiles.git ~/.dotfiles
	./.dotfiles/install

## Defined Aliases

See `aliases` file.
