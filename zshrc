include () {
	[[ -f "$1" ]] && source "$1"
}

ZSH_THEME="powerline"
POWERLINE_DISABLE_RPROMPT="true"

export PATH=$PATH:$HOME/.dotfiles/scripts
export VIMRUNTIME=/usr/share/vim/vim80

include ~/.dotfiles/aliases
include ~/.env
